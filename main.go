package main

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/memoshnaya/bot/database"
	"gitlab.com/memoshnaya/bot/handlers"
	"golang.org/x/net/proxy"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Println("bot")

	dbUser := os.Getenv("DATABASE_USER")
	dbName := os.Getenv("DATABASE")
	dbPass := os.Getenv("DATABASE_PASSWORD")

	db, err := database.NewDB(dbUser, dbName, dbPass)
	if err != nil {
		log.Fatalf("could not connect db: %v", err)
	}
	log.Println("database connected")

	var (
		bot     *tgbotapi.BotAPI
		updates tgbotapi.UpdatesChannel
	)

	token := os.Getenv("BOT_TOKEN")
	if token == "" {
		log.Fatal("could not get token from env (BOT_TOKEN)")
	}

	useWebhook := os.Getenv("BOT_USE_WEBHOOK")
	if useWebhook == "t" {
		bot, err = tgbotapi.NewBotAPI(token)
		if err != nil {
			log.Fatalf("could not create bot: %v", err)
		}
		bot.Debug = true
		log.Printf("Authorized on account %s", bot.Self.UserName)

		srvAddr := os.Getenv("SRV_ADDR")
		if srvAddr == "" {
			log.Fatal("server addr not set")
		}

		_, err = bot.SetWebhook(tgbotapi.NewWebhookWithCert(fmt.Sprintf("https://%s/%s", srvAddr, bot.Token), "/etc/ssl/certs/bot.pem"))
		if err != nil {
			log.Fatalf("could not set webhook: %v", err)
		}

		info, err := bot.GetWebhookInfo()
		if err != nil {
			log.Fatalf("could not get webhook info: %v", err)
		}
		if info.LastErrorDate != 0 {
			log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
		}

		updates = bot.ListenForWebhook("/" + bot.Token)
		go func() {
			err = http.ListenAndServe(":8080", nil)
			if err != nil {
				log.Fatalf("could not listen 8080: %v", err)
			}
		}()
	} else {
		dialer, err := proxy.SOCKS5("tcp", "198.27.75.152:1080", nil, proxy.Direct)
		if err != nil {
			log.Fatalf("could not create SOCKS5 dialer: %v", err)
		}

		bot, err = tgbotapi.NewBotAPIWithClient(token, &http.Client{
			Transport: &http.Transport{
				Dial: dialer.Dial,
			},
		})
		if err != nil {
			log.Fatalf("could not create bot api: %v", err)
		}

		res, err := bot.RemoveWebhook()
		if err != nil {
			log.Fatalf("could not remove webhook: %v", err)
		}

		log.Println(res)

		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60
		updates, err = bot.GetUpdatesChan(u)
		if err != nil {
			log.Fatalf("could not get u chan: %v", err)
		}
	}

	h := handlers.NewHandler(db, bot)

	log.Println("ok bot started")

	for u := range updates {
		if u.Message != nil {
			go h.Message(u.Message)
			continue
		}

		if u.CallbackQuery != nil {
			go h.CallbackQuery(u.CallbackQuery)
			continue
		}

	}
}
