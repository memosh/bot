migrate:
	docker run --rm -v $(shell pwd)/migrations:/migrations --network host migrate/migrate -path /migrations/ -database 'postgres://postgres:password@localhost:5432/example?sslmode=disable' up

psql:
	docker run -it --network host postgres psql -h localhost -U postgres -d example
