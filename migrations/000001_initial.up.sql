create table if not exists chats
(
    chat_id int primary key,
    type    text not null,
    title   text,

    active  boolean   default true,

    created timestamp default now()
);

create table if not exists users
(
    user_id    int primary key,
    username   text,
    first_name text,
    last_name  text,
    created    timestamp default now()
);

create table if not exists chat_users
(
    user_id int references users (user_id) on delete cascade on update cascade,
    chat_id int references chats (chat_id) on delete cascade on update cascade,
    primary key (user_id, chat_id)
);

create table if not exists posts
(
    id         serial primary key,
    message_id int  not null,
    file_id    text not null,
    user_id    int  references users (user_id) on update cascade on delete set null,
    chat_id    int  references chats (chat_id) on update cascade on delete set null,
    created    timestamp default now(),
    unique (message_id, chat_id)
);

create table if not exists reactions
(
    user_id int  references users (user_id) on update cascade on delete set null,
    post_id int references posts (id) on update cascade on delete cascade,
    type    text not null,
    created timestamp default now(),
    primary key (user_id, post_id)
);
