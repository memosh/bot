package handlers

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/memoshnaya/bot/database"
	"log"
	"strings"
	"time"
)

// GroupMessage handles group and supergroups messages
func (h *Handler) GroupMessage(m *tgbotapi.Message) error {
	switch {
	case m.NewChatMembers != nil:
		if err := h.newMembers(m); err != nil {
			log.Printf("new members err: %v\n", err)
		}
	case m.LeftChatMember != nil:
		if err := h.leftMember(m); err != nil {
			log.Printf("left members err: %v\n", err)
		}
	case m.IsCommand():
		h.todo(m)
	case m.Photo != nil:
		if err := h.userPhoto(m); err != nil {
			log.Printf("could not handle photo: %v", err)
		}
	case m.Video != nil:
		if err := h.userVideo(m); err != nil {
			log.Printf("could not handle video: %v", err)
		}
	case m.Animation != nil:
		if err := h.userAnimation(m); err != nil {
			log.Printf("could not handle video: %v", err)
		}
	case m.Text != "":
		if err := h.userText(m); err != nil {
			log.Printf("could not handle text: %v", err)
		}
	default:
		h.todo(m)
	}

	return nil
}

func (h *Handler) userPhoto(m *tgbotapi.Message) error {
	fileId := (*m.Photo)[0].FileID
	msg := tgbotapi.NewPhotoShare(m.Chat.ID, fileId)
	msg.ReplyMarkup = newReactionsKeyboard(0, 0)
	msg.DisableNotification = true
	msg.Caption = "from: " + from(m.From)
	if m.Caption != "" {
		msg.Caption = fmt.Sprintf("%s\n%s", m.Caption, msg.Caption)
	}
	if m.ReplyToMessage != nil {
		msg.ReplyToMessageID = m.ReplyToMessage.MessageID
	}

	return h.newUserMediaPost(m, msg, fileId)
}

func (h *Handler) userVideo(m *tgbotapi.Message) error {
	fileId := m.Video.FileID
	msg := tgbotapi.NewVideoShare(m.Chat.ID, fileId)
	msg.ReplyMarkup = newReactionsKeyboard(0, 0)
	msg.DisableNotification = true
	msg.Caption = "from: " + from(m.From)
	if m.Caption != "" {
		msg.Caption = fmt.Sprintf("%s\n%s", m.Caption, msg.Caption)
	}
	if m.ReplyToMessage != nil {
		msg.ReplyToMessageID = m.ReplyToMessage.MessageID
	}

	return h.newUserMediaPost(m, msg, fileId)
}

func (h *Handler) userAnimation(m *tgbotapi.Message) error {
	fileId := m.Animation.FileID
	msg := tgbotapi.NewAnimationShare(m.Chat.ID, fileId)
	msg.ReplyMarkup = newReactionsKeyboard(0, 0)
	msg.DisableNotification = true
	msg.Caption = "from: " + from(m.From)
	if m.Caption != "" {
		msg.Caption = fmt.Sprintf("%s\n%s", m.Caption, msg.Caption)
	}
	if m.ReplyToMessage != nil {
		msg.ReplyToMessageID = m.ReplyToMessage.MessageID
	}

	return h.newUserMediaPost(m, msg, fileId)
}

func newReactionsKeyboard(positive, negative int) tgbotapi.InlineKeyboardMarkup {
	return tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData(fmt.Sprintf("👍 %d", positive), "p"),
			tgbotapi.NewInlineKeyboardButtonData(fmt.Sprintf("👎 %d", negative), "n"),
		))
}

func from(user *tgbotapi.User) string {
	if user.UserName != "" {
		return "@" + user.UserName
	}

	f := user.FirstName
	if user.LastName != "" {
		f = fmt.Sprintf("%s %s", f, user.LastName)
	}
	return f
}

func (h *Handler) newUserMediaPost(m *tgbotapi.Message, msg tgbotapi.Chattable, fileID string) error {
	if err := h.db.CreateChatIfNotExist(database.NewChat(m.Chat)); err != nil {
		return fmt.Errorf("could not check chat: %w", err)
	}

	if err := h.db.AddUserToChat(m.Chat.ID, database.NewUser(m.From)); err != nil {
		return fmt.Errorf("could not add usr: %w", err)
	}

	deleteMsg := tgbotapi.NewDeleteMessage(m.Chat.ID, m.MessageID)
	if _, err := h.bot.Send(deleteMsg); err != nil {
		return fmt.Errorf("could not delete msg: %w", err)
	}

	sentMsg, err := h.bot.Send(msg)
	if err != nil {
		return fmt.Errorf("could not send new post: %w", err)
	}

	err = h.db.CreatePost(&database.Post{
		MessageId: sentMsg.MessageID,
		FileId:    fileID,
		UserId:    m.From.ID,
		ChatId:    m.Chat.ID,
	})
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) newMembers(m *tgbotapi.Message) error {
	if !h.botAdded(m) {
		return nil
	}

	return h.db.CreateOrActivateChat(database.NewChat(m.Chat))
}

func (h *Handler) leftMember(m *tgbotapi.Message) error {
	if m.LeftChatMember == nil || m.LeftChatMember.ID != h.bot.Self.ID {
		return nil
	}
	return h.db.DeactivateChat(&database.Chat{Id: m.Chat.ID})
}

func (h *Handler) botAdded(m *tgbotapi.Message) bool {
	for _, newMember := range *m.NewChatMembers {
		if newMember.ID == h.bot.Self.ID {
			return true
		}
	}
	return false
}

func (h *Handler) userText(m *tgbotapi.Message) error {
	if m.Text == "" {
		return nil
	}

	if m.Entities == nil {
		return h.textWarning(m)
	}

	if err := h.db.CreateChatIfNotExist(database.NewChat(m.Chat)); err != nil {
		return fmt.Errorf("could not check chat: %w", err)
	}

	if err := h.db.AddUserToChat(m.Chat.ID, database.NewUser(m.From)); err != nil {
		return fmt.Errorf("could not add usr: %w", err)
	}

	deleteMsg := tgbotapi.NewDeleteMessage(m.Chat.ID, m.MessageID)
	if _, err := h.bot.Send(deleteMsg); err != nil {
		return fmt.Errorf("could not delete msg: %w", err)
	}

	e := (*m.Entities)[0]
	if e.Type != "url" {
		return nil
	}

	eURL := string([]rune(m.Text)[e.Offset : e.Offset+e.Length])
	urlName := "Ссылочка"
	switch {
	case strings.Contains(eURL, "youtube"):
		urlName = "Видос"
	case strings.Contains(eURL, "coub"):
		urlName = "coub"
	case strings.Contains(eURL, "twitter"):
		urlName = "Лукоедская ссылочка"
	}

	linkText := fmt.Sprintf(
		"[%s](%s) _от_ %s",
		urlName,
		eURL,
		from(m.From),
	)
	caption := string([]rune(m.Text)[:e.Offset])
	if e.Offset == 0 {
		caption = string([]rune(m.Text)[e.Offset+e.Length:])
	}
	if caption != "" {
		linkText = fmt.Sprintf("%s\n%s", caption, linkText)
	}

	msg := tgbotapi.NewMessage(m.Chat.ID, linkText)
	msg.ParseMode = tgbotapi.ModeMarkdown
	msg.DisableNotification = true
	msg.ReplyMarkup = newReactionsKeyboard(0, 0)
	if m.ReplyToMessage != nil {
		msg.ReplyToMessageID = m.ReplyToMessage.MessageID
	}
	sentMsg, _ := h.bot.Send(msg)

	err := h.db.CreatePost(&database.Post{
		MessageId: sentMsg.MessageID,
		FileId:    "",
		UserId:    m.From.ID,
		ChatId:    m.Chat.ID,
	})
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) textWarning(m *tgbotapi.Message) error {
	deleteMsg := tgbotapi.NewDeleteMessage(m.Chat.ID, m.MessageID)
	if _, err := h.bot.Send(deleteMsg); err != nil {
		return fmt.Errorf("could not delete msg: %w", err)
	}

	msg := tgbotapi.NewMessage(
		m.Chat.ID,
		from(m.From)+", похоже ты не прислал мем, а просто написал что-то\nУ нас тут так не делается")
	msg.DisableNotification = true
	sentMsg, err := h.bot.Send(msg)
	if err != nil {
		return fmt.Errorf("could not send text warning: %v", err)
	}

	go func(id int) {
		t := time.NewTimer(15 * time.Second)
		<-t.C

		_, e := h.bot.Send(tgbotapi.NewDeleteMessage(m.Chat.ID, id))
		if e != nil {
			log.Println(e)
		}
	}(sentMsg.MessageID)

	return nil
}
