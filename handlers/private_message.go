package handlers

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

// PrivateMessage handles messages sent to the bot
func (h *Handler) PrivateMessage(m *tgbotapi.Message) error {
	h.todo(m)
	return nil
}
