package handlers

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/memoshnaya/bot/database"
	"log"
)

type Handler struct {
	db  *database.DB
	bot *tgbotapi.BotAPI
}

func NewHandler(db *database.DB, bot *tgbotapi.BotAPI) *Handler {
	return &Handler{db: db, bot: bot}
}

func (h *Handler) todo(m *tgbotapi.Message) {
	log.Printf("From: %s message: %s\n", m.From.UserName, m.Text)
	user, err := h.db.UpsertUser(database.NewUser(m.From))
	log.Printf("user: %#v", user)
	msg := tgbotapi.NewMessage(m.Chat.ID, "Not implemented. "+m.Text)
	_, err = h.bot.Send(msg)
	if err != nil {
		log.Printf("todo error: %v", err)
	}
	return
}

func (h *Handler) Message(m *tgbotapi.Message) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	if m == nil {
		return
	}

	switch {
	case m.Chat.IsPrivate():
		if err := h.PrivateMessage(m); err != nil {
			log.Printf("could not handle private msg: %v", err)
		}
	case m.Chat.IsGroup() || m.Chat.IsSuperGroup():
		if err := h.GroupMessage(m); err != nil {
			log.Printf("could not handle group msg: %v", err)
		}
	case m.Chat.IsChannel():
		log.Println("channel message")
	}
}

func (h *Handler) CallbackQuery(q *tgbotapi.CallbackQuery) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	if q == nil || q.Message == nil {
		return
	}

	switch {
	case q.Message.Chat.IsGroup() || q.Message.Chat.IsSuperGroup():
		if err := h.GroupCallbackQuery(q); err != nil {
			log.Printf("could not handle group callback query: %v", err)
		}
	default:
	}
}
