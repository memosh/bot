package handlers

import (
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/memoshnaya/bot/database"
	"math/rand"
	"time"
)

var (
	positiveEmoji   = []string{"👌", "💪", "👍", "🤘", "🔥"}
	negativeEmoji   = []string{"🤦‍♂️", "🤦‍♀️", "🥴", "💩", "👹"}
	positiveAnswers = []string{"Топчик", "Хорошо", "Кайф", "Огонь", "Пушка"}
	negativeAnswers = []string{"Не очень", "Э:)"}
)

func (h *Handler) GroupCallbackQuery(q *tgbotapi.CallbackQuery) error {
	if q == nil {
		return errors.New("query is nil")
	}

	_, _ = h.bot.AnswerCallbackQuery(tgbotapi.NewCallback(q.ID, randomCallbackAnswerText(q.Data)))

	t := database.Positive
	if q.Data == "n" {
		t = database.Negative
	}
	reaction, err := h.db.UpsertReaction(q.From.ID, q.Message.MessageID, q.Message.Chat.ID, t)
	if err != nil {
		return fmt.Errorf("could not create or update reation: %w", err)
	}

	counts, err := h.db.GetReactionsCount(reaction.PostId)
	if err != nil {
		return fmt.Errorf("could not get reactions  count: %w", err)
	}
	newKb := newReactionsKeyboard(counts.Positive, counts.Negative)
	_, err = h.bot.Send(tgbotapi.NewEditMessageReplyMarkup(q.Message.Chat.ID, q.Message.MessageID, newKb))
	if err != nil {
		return fmt.Errorf("could not update reply markup: %w", err)
	}

	return nil
}

func randomCallbackAnswerText(data string) string {
	rand.Seed(time.Now().Unix())
	reply := positiveAnswers[rand.Intn(len(positiveAnswers))] + " " + positiveEmoji[rand.Intn(len(positiveEmoji))]
	if data == "n" {
		reply = negativeAnswers[rand.Intn(len(negativeAnswers))] + " " + negativeEmoji[rand.Intn(len(negativeEmoji))]
	}
	return reply
}
