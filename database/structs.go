package database

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"time"
)

type Chat struct {
	Id     int64  `db:"chat_id"`
	Type   string `db:"type"`
	Title  string `db:"title"`
	Active bool   `db:"active"`

	Created time.Time `db:"created"`
}

func NewChat(c *tgbotapi.Chat) *Chat {
	return &Chat{
		Id:    c.ID,
		Type:  c.Type,
		Title: c.Title,
	}
}

type User struct {
	Id        int    `db:"user_id"`
	Username  string `db:"username"`
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`

	Created time.Time `db:"created"`
}

func NewUser(u *tgbotapi.User) *User {
	return &User{
		Id:        u.ID,
		Username:  u.UserName,
		FirstName: u.FirstName,
		LastName:  u.LastName,
	}
}

type Post struct {
	Id        int       `db:"id"`
	MessageId int       `db:"message_id"`
	FileId    string    `db:"file_id"`
	UserId    int       `db:"user_id"`
	ChatId    int64     `db:"chat_id"`
	Created   time.Time `db:"created"`
}

type reactionType string

const (
	Positive reactionType = "p"
	Negative reactionType = "n"
)

type Reaction struct {
	UserId  int       `db:"user_id"`
	PostId  int       `db:"post_id"`
	Type    string    `db:"type"`
	Created time.Time `db:"created"`
}

type ReactionsCount struct {
	Positive int
	Negative int
}
