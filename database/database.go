package database

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"

	_ "github.com/lib/pq"
)

type DB struct {
	db *sqlx.DB
}

func NewDB(user, database, password string) (*DB, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf(
		"password=%s user=%s dbname=%s sslmode=disable",
		password,
		user,
		database,
	))
	if err != nil {
		log.Fatalf("could not connect to pg: %v", err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("unsuccessful db ping: %w", err)
	}

	return &DB{db: db}, nil
}

func (d *DB) Ping() error {
	return d.db.Ping()
}

func (d *DB) CreateOrActivateChat(c *Chat) error {
	_, err := d.db.NamedExec(`
		insert into chats (chat_id, type, title) values (:chat_id, :type, :title)  
		on conflict on constraint chats_pkey do update set active = true, title=:title;`, c)
	if err != nil {
		return fmt.Errorf("could not create or activate chat: %v", err)
	}
	return nil
}

func (d *DB) DeactivateChat(c *Chat) error {
	_, err := d.db.NamedExec(`update chats set active = false where chat_id=:chat_id;`, c)
	if err != nil {
		return fmt.Errorf("could not deactivate chat: %w", err)
	}
	return nil
}

func (d *DB) CreateChatIfNotExist(c *Chat) error {
	var chatExists bool
	if err := d.db.Get(&chatExists, `select exists(select 1 from chats where chat_id = $1)`, c.Id); err != nil {
		return err
	}

	if chatExists {
		return nil
	}

	return d.CreateOrActivateChat(c)
}

func (d *DB) UpsertUser(user *User) (*User, error) {
	rows, err := d.db.NamedQuery(`
		insert into users (user_id, username, first_name, last_name) 
		values (:user_id, :username, :first_name, :last_name) 
		on conflict on constraint users_pkey 
		    do update set username=:username, first_name=:first_name, last_name=:last_name 
		returning *`, user)
	if err != nil {
		return nil, fmt.Errorf("could not upsert user: %w", err)
	}
	defer rows.Close()

	if !rows.Next() {
		return nil, errors.New("something went wrong")
	}

	if err := rows.StructScan(user); err != nil {
		return nil, fmt.Errorf("could not scan user: %w", err)
	}

	return user, nil
}

func (d *DB) AddUserToChat(chatID int64, user *User) error {
	user, err := d.UpsertUser(user)
	if err != nil {
		return fmt.Errorf("add user to chat err: %w", err)
	}

	_, err = d.db.Exec(`
		insert into chat_users (user_id, chat_id) values ($1, $2) on conflict do nothing`,
		user.Id, chatID)
	if err != nil {
		return fmt.Errorf("could not add user to chat: %w", err)
	}
	return nil
}

func (d *DB) CreatePost(p *Post) error {
	_, err := d.db.NamedExec(`
		insert into posts (message_id, file_id, user_id, chat_id) 
		values (:message_id, :file_id, :user_id, :chat_id)`, p)
	if err != nil {
		return fmt.Errorf("could not insert post: %w", err)
	}
	return nil
}

func (d *DB) UpsertReaction(userId int, messageId int, chatId int64, t reactionType) (*Reaction, error) {
	reaction := &Reaction{}
	err := d.db.QueryRowx(`
		insert into reactions (user_id, post_id, type) 
		values ($1, (
		    select id from posts where message_id = $2 and chat_id = $3
		), $4) 
		on conflict on constraint reactions_pkey do update set type=$4 
		returning *;`,
		userId, messageId, chatId, t).StructScan(reaction)
	if err != nil {
		return nil, fmt.Errorf("could not create reaction: %w", err)
	}
	return reaction, nil
}

func (d *DB) GetReactionsCount(postId int) (*ReactionsCount, error) {
	reactionsCount := &ReactionsCount{}
	err := d.db.QueryRow(`select count(*) from reactions where post_id=$1 and type = $2`, postId, Positive).
		Scan(&reactionsCount.Positive)
	if err != nil {
		return nil, err
	}

	err = d.db.QueryRow(`select count(*) from reactions where post_id=$1 and type = $2`, postId, Negative).
		Scan(&reactionsCount.Negative)
	if err != nil {
		return nil, err
	}

	return reactionsCount, nil
}
